import java.util.*;
public class Roulette{

    public static RouletteWheel wheel = new RouletteWheel();

    public static Scanner scan = new Scanner(System.in);

    public static int bet(int usermoney){
        System.out.println("how much would you like to bet?");
        int bet = scan.nextInt();
        boolean works = false;
        while(!works){
            if(usermoney >= bet)
            {
                works = true;
            }
            else{
                System.out.println("how much would you like to bet?");
                bet = scan.nextInt();
            }
        }
        return bet;
    }

    public static int spin(){
        wheel.spin();
        return wheel.getVaue();
    }

    public static int userBetNum(){
        boolean works = false;
        int bet = 0;
        System.out.println("what number would you like to bet on?");
        bet = scan.nextInt();
        while (!works){
            if(bet >= 0 && bet < 37)
            {
                works = true;
            }
            else{
                System.out.println("what number would you like to bet on?");
                bet = scan.nextInt();
            }
        }
        return bet;
    }

    public static int userWin(int num, int usermoney, int userBet){
        int lucky = spin();
        if(lucky == num){
            int old = userBet;
            System.out.println("You won");
            return usermoney+(userBet*35+old);
        }
        else{
            System.out.println("You lost :()");
            return usermoney-userBet;
        }
    }
    public static void main(String[]args){
        
        int usermoney = 1000;
        boolean play = false;
        while(!play){
            System.out.println("You have: " + usermoney + "$");
            int userBet = bet(usermoney);
            int userNum = userBetNum();
            usermoney = userWin(userNum, usermoney, userBet);
            if(usermoney < 1){
                System.out.println("sorry you have no money");
                play = true;
            }
            else{
                System.out.println("would you like to play again (y/n)");
                String answer = scan.next();
                if(answer.equals("n")){
                    play = true;
                }
            }
        }
    }
}